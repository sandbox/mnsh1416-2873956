<?php

namespace Drupal\youtube_uploader\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'youtube_file_default' formatter.
 *
 * @FieldFormatter(
 *   id = "youtube_file_default",
 *   label = @Translation("Youtube Upload"),
 *   field_types = {
 *     "Youtubefile"
 *   }
 * )
 */
class YoutubeFileFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      if ($item) {
        $options = [
          'src' => 'https://www.youtube.com/v/' . $item->you_videoid . '?version=3&autoplay=1',
          'vid' => $item->you_videoid,
          'title' => $item->you_title,
          'desc' => $item->you_desc,
          'tags' => $item->you_tags,
        ];
        $elements[$delta] = [
          '#theme' => 'youtube_uploader_field_formatter',
          '#options' => $options,
        ];
      }
    }
    return $elements;
  }

}
