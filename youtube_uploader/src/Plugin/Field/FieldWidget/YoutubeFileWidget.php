<?php

namespace Drupal\youtube_uploader\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\FIeld\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'youtube_file_generic' widget.
 *
 * @FieldWidget(
 *   id = "youtube_file_generic",
 *   label = @Translation("Youtube File"),
 *   field_types = {
 *     "Youtubefile",
 *   }
 * )
 */
class YoutubeFileWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('youtube_uploader.settings');
    $item = $items[$delta];

    // If Video is available.
    if ($item->get('you_videoid')->getValue()) {
      $youtube_video_data = '';
      if ($config->get('yt_show_title')) {
        $youtube_video_data .= '<h2>' . $item->get('you_title')->getValue() . '</h2>';
      }

      if ($item->get('you_title')->getValue()) {
        $youtube_video_data .= '<iframe width = "500" height = "320" frameborder = "0" allowfullscreen = "allowfullscreen" src = "https://www.youtube.com/embed/' . $item->get('you_videoid')->getValue() . '?autoplay=0&amp;start=0&amp;rel=0"></iframe>';
      }

      if ($config->get('yt_show_tags')) {
        $youtube_video_data .= '<div><strong>' . $item->get('you_tags')->getValue() . '</strong></div>';
      }

      if ($config->get('yt_show_desc')) {
        $youtube_video_data .= '<p>' . $item->get('you_desc')->getValue() . '</p>';
      }

      $element['youtube_uploader_video'] = [
        '#markup' => $youtube_video_data,
        '#allowed_tags' => ['iframe', 'h2', 'p', 'strong', 'div'],
      ];
    }
    else {

      if ($config->get('yt_show_title')) {
        $element['you_title'] = [
          '#type' => 'textfield',
          '#title' => 'Title of Video',
          '#weight' => -10,
          '#default_value' => ($item->get('you_title')->getValue()) ? $item->get('you_title')->getValue() : '',
          '#description' => $this->t('This Title will be the title for your video on YouTube.'),
          '#attributes' => [
            'class' => ['youtube-title'],
          ],
        ];
      }

      if ($config->get('yt_show_desc')) {
        $element['you_desc'] = [
          '#type' => 'textarea',
          '#format' => 'filtered_html',
          '#rows' => 10,
          '#title' => 'Description of Video',
          '#weight' => -9,
          '#default_value' => ($item->get('you_desc')->getValue()) ? $item->get('you_desc')->getValue() : '',
          '#description' => $this->t('Description for your video on YouTube.'),
          '#attributes' => [
            'class' => ['youtube-desc'],
          ],
        ];
      }

      if ($config->get('yt_show_tags')) {
        $element['you_tags'] = [
          '#type' => 'textfield',
          '#title' => 'Tags of Video',
          '#weight' => -8,
          '#default_value' => ($item->get('you_tags')->getValue()) ? $item->get('you_tags')->getValue() : '',
          '#description' => $this->t('Provides Youtube Video Tags Field on Youtube (comma separated Eg: "thrill, amazing").'),
          '#attributes' => [
            'class' => ['youtube-tags'],
          ],
        ];
      }

      $element['choose'] = [
        '#name' => 'files[youtube_video]',
        '#type' => 'file',
        '#title' => $this->t('Select Video'),
        '#weight' => -7,
        '#description' => $this->t('This video will directly uploaded to YouTube'),
        '#size' => 20,
        '#suffix' => '<div class="during-upload" style="display: none"> <progress id="upload-progress" max="1" value="0"></progress> <span id="percent-transferred"></span>% ( <span id="seconds-left"></span> sec left )</div>',
        '#attributes' => [
          'class' => ['youtube-uploader-file'],
        ],
      ];

      $element['upload'] = [
        '#markup' => '<a href="#" class="upload-on-youtube button">' . $this->t('Upload') . '</a>',
        '#weight' => -6,
        '#attributes' => [
          'class' => ['upload-on-youtube'],
        ],
      ];

      $element['you_videoid'] = [
        '#type' => 'hidden',
        '#weight' => -4,
        '#default_value' => ($item->get('you_videoid')->getValue()) ? $item->get('you_videoid')->getValue() : '',
        '#attributes' => [
          'class' => ['youtube_hidden_video_id youtube_hidden_id'],
        ],
      ];
    }

    $element += [
      '#type' => 'fieldset',
      '#description' => $this->t('This video will get upload to YouTube'),
      '#prefix' => '<div class="field-type-youtube-upload">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => ['file-widget form-managed-file'],
      ],
    ];

    $element['#attached']['library'][] = 'youtube_uploader/youtube_uploader';

    return $element;
  }

}
