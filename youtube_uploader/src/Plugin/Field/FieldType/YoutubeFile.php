<?php

namespace Drupal\youtube_uploader\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Plugin implementation of the 'Youtubefile' field type.
 *
 * @FieldType(
 *   id = "Youtubefile",
 *   label = @Translation("Youtube File"),
 *   description = @Translation("This field stores the ID of a Youtube file."),
 *   category = @Translation("Reference"),
 *   default_widget = "youtube_file_generic",
 *   default_formatter = "youtube_file_default",
 * )
 */
class YoutubeFile extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        // Youtube Video ID.
        'you_videoid' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
        // Youtube Title.
        'you_title' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
        // Youtube Description.
        'you_desc' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
        // Youtube Tags.
        'you_tags' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['you_videoid'] = DataDefinition::create('string')->setLabel(t('YouTube Video ID'));
    $properties['you_title'] = DataDefinition::create('string')->setLabel(t('YouTube Video Title'));
    $properties['you_desc'] = DataDefinition::create('string')->setLabel(t('YouTube Video Description'));
    $properties['you_tags'] = DataDefinition::create('string')->setLabel(t('YouTube Video Tags'));
    return $properties;
  }

}
