<?php

namespace Drupal\youtube_uploader\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config form.
 */
class YoutubeUploadConfigForm extends ConfigFormBase {

  protected $ytservice;

  /**
   * {@inheritdoc}
   */
  public function __construct($youtube_uploaderYoutube) {
    $this->ytservice = $youtube_uploaderYoutube;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('youtube_uploader_service'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_uploader_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['youtube_uploader.settings'];
  }

  /**
   * Check is item empty.
   */
  public function isEmpty($item) {
    return ($item === NULL || $item === '');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    // Get config.
    $config = $this->config('youtube_uploader.settings');
    $form['authentication'] = [
      '#type' => 'details',
      '#title' => 'Google Authentication',
      '#description' => $this->t('Youtube Upload requires Google Account authentication to upload videos to YouTube.'),
      '#open' => TRUE,
    ];
    $form['credentials'] = [
      '#type' => 'details',
      '#title' => 'Credentials',
      '#open' => TRUE,
    ];
    $hasAccessToken = $config->get('access_token');
    if ($hasAccessToken) {
      $authorized = '<p>Status: <strong>Authorized</strong>.</p><p><a class="button" href="' . $base_url . '/youtube_uploader/revoke">Revoke Current Authentication</a></a>';
      $form['authentication']['youtube_uploader_access_token'] = [
        '#type' => 'markup',
        '#markup' => $authorized,
      ];

      // Channel Details.
      $form['channel'] = [
        '#type' => 'details',
        '#title' => 'YouTube Details',
        '#open' => FALSE,
      ];

      $channelSettings = $this->ytservice->youTubeAccount();

      $details = '<p><strong>Channel Name:</strong> ' . $channelSettings->title . '.</p>';
      $details .= '<p><strong>Channel Description:</strong> ' . $channelSettings->description . '.</p>';
      $details .= '<p><strong>Channel Keywords:</strong> ' . $channelSettings->keywords . '.</p>';
      $form['channel']['details'] = [
        '#type' => 'markup',
        '#markup' => $details,
      ];

      $form['authentication']['#open'] = FALSE;
      $form['credentials']['#open'] = FALSE;
    }
    else {
      $hasClientIds = $config->get('client_id');
      $hasClientSecret = $config->get('client_secret');
      $hasRedirectUri = $config->get('redirect_uri');

      if (!$this->isEmpty($hasClientIds) && !$this->isEmpty($hasClientSecret) && !$this->isEmpty($hasRedirectUri)) {
        $auth_url = $this->ytservice->getAuthUrl();
        $unauthorized = '<p>Status: <strong>Unauthorized</strong>.</p><p><a class="button" href="' . $auth_url . '">Authorize</a></p>';
        $form['authentication']['authorize'] = [
          '#type' => 'markup',
          '#markup' => $unauthorized,
        ];
      }
      else {
        $status = '<p>Status: <strong>Credentials required</strong>.</p><p>Provide values for Client ID, Secret and Redirect Uri</p>';
        $form['authentication']['authorize'] = [
          '#type' => 'markup',
          '#markup' => $status,
        ];
      }
    }

    $form['credentials']['youtube_uploader_client_id'] = [
      '#type' => 'textfield',
      '#title' => 'Client ID',
      '#default_value' => $config->get('client_id'),
      '#description' => $this->t('Set Client Id'),
      '#disabled' => $hasAccessToken,
    ];

    $form['credentials']['youtube_uploader_client_secret'] = [
      '#type' => 'textfield',
      '#title' => 'Client Secret',
      '#default_value' => $config->get('client_secret'),
      '#description' => $this->t('Set Client Secret'),
      '#disabled' => $hasAccessToken,
    ];

    $redirect_uri = $base_url . '/youtube_uploader/authorize';
    $form['credentials']['youtube_uploader_redirect_uri'] = [
      '#type' => 'textfield',
      '#title' => 'Redirect uri',
      '#default_value' => ($config->get('redirect_uri')) ? $config->get('redirect_uri') : $redirect_uri,
      '#description' => $this->t("Redirect uri should be set to '%redirect_uri'", ['%redirect_uri' => $redirect_uri]),
      '#disabled' => $hasAccessToken,
    ];

    // Allowed Fields along with Video.
    $form['allowed-options'] = [
      '#type' => 'details',
      '#title' => 'Allowed Fields for Editor/End User',
      '#open' => TRUE,
    ];

    $form['allowed-options']['youtube_show_title'] = [
      '#type' => 'checkbox',
      '#title' => 'Youtube Video Title',
      '#default_value' => $config->get('yt_show_title'),
      '#description' => $this->t('Provides Youtube Video Title Field on Youtube else Node title will be treated as Video Title'),
    ];

    $form['allowed-options']['youtube_show_desc'] = [
      '#type' => 'checkbox',
      '#title' => 'Youtube Video Description',
      '#default_value' => $config->get('yt_show_desc'),
      '#description' => $this->t('Provides Youtube Video Description Field on Youtube.'),
    ];

    $form['allowed-options']['youtube_show_tags'] = [
      '#type' => 'checkbox',
      '#title' => 'Youtube Video Tags',
      '#default_value' => $config->get('yt_show_tags'),
      '#description' => $this->t('Provides Youtube Video Tags Field on Youtube (comma separated Eg: "thrill, amazing").'),
    ];

    $options = [
      'public' => 'Public',
      'private' => 'Private',
      'unlisted' => 'Unlisted',
    ];

    $form['youtube_default_privacy'] = [
      '#type' => 'select',
      '#title' => 'Default Privacy of Video',
      '#options' => $options,
      '#default_value' => $config->get('yt_default_privacy'),
      '#description' => $this->t('Default Privacy of videos uploaded on youtube. For more info: <a href="https://support.google.com/youtube/answer/157177?co=GENIE.Platform%3DDesktop&hl=en" target="new">Click Here </a>'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('youtube_uploader.settings')
      ->set('client_id', $values['youtube_uploader_client_id'])
      ->set('client_secret', $values['youtube_uploader_client_secret'])
      ->set('redirect_uri', $values['youtube_uploader_redirect_uri'])
      ->set('yt_show_title', $values['youtube_show_title'])
      ->set('yt_show_desc', $values['youtube_show_desc'])
      ->set('yt_show_tags', $values['youtube_show_tags'])
      ->set('yt_default_privacy', $values['youtube_default_privacy'])
      ->save();
  }

}
