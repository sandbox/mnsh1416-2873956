<?php

namespace Drupal\youtube_uploader;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * YouTube Service.
 */
class YouTubeService {

  /**
   * Protected configFactory variable.
   *
   * @var configFactory
   */
  protected $configFactory;
  protected $clientId;
  protected $clientSecret;
  protected $redirectUri;
  protected $client;

  /**
   * Constructor for YouTube service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    try {
      if (!$this->getCredentials()) {
        // Credentials present.
        return FALSE;
      }
      // Set client.
      $this->client = new \Google_Client();

      // Initialize client.
      $this->client->setClientId($this->client_id);
      $this->client->setClientSecret($this->client_secret);
      $this->client->setScopes('https://www.googleapis.com/auth/youtube');
      $this->client->setRedirectUri($this->redirect_uri);
      // These two are required to get refresh_token.
      $this->client->setAccessType("offline");
      $this->client->setApprovalPrompt("force");
    }
    catch (\Exception $e) {
      drupal_set_message($this->t('youtube_uploader Error : @e', ['@e' => $e->getMessage()]), 'error');
    }
  }

  /**
   * Returns if all the 3 credentials available.
   */
  public function getCredentials() {
    $clientId = $this->getConfig('client_id');
    $clientSecret = $this->getConfig('client_secret');
    $redirectUri = $this->getConfig('redirect_uri');

    if (isset($clientId) && isset($clientSecret) && isset($redirectUri)) {
      // Credentials present.
      $this->client_id = $clientId;
      $this->client_secret = $clientSecret;
      $this->redirect_uri = $redirectUri;
      return [
        'client_id' => $clientId,
        'client_secret' => $clientSecret,
        "redirect_uri" => $redirectUri,
      ];
    }
    else {
      drupal_set_message('Youtube_upload\YouTubeService: Credentials not present.', 'warning');
      return FALSE;
    }
  }

  /**
   * Manages google token.
   */
  public function manageTokens() {
    // Calculate token expiry.
    $token = $this->getConfig('access_token');
    $this->client->setAccessToken($token);
    // And perform required action.
    if ($this->client->isAccessTokenExpired()) {
      // If Token expired.
      // we need to refresh token in this case.
      // Check whether we have a refresh token or not.
      $refreshToken = $this->getConfig('refresh_token');
      if ($refreshToken != NULL) {
        // If refresh token present.
        $this->client->refreshToken($refreshToken);
        $newToken = $this->client->getAccessToken();
        $config = $this->configFactory->getEditable('youtube_uploader.settings');
        $config->set('access_token', $newToken)->save();
      }
      else {
        // If refresh token isn't present.
        $this->client->refreshToken($refreshToken);
        $newToken = $this->client->getAccessToken();
        $config = $this->configFactory->getEditable('youtube_uploader.settings');
        $config->set('access_token', $newToken)->save();
      }
    }
  }

  /**
   * Get default thumb and actual title for a video.
   */
  public function getTitleThumbs($video_id) {

    $this->getFreshToken();
    $list_response = $this->yt->videos->listVideos("snippet", ['id' => $video_id]);

    if (empty($list_response)) {
      return ['error' => $this->t('Video %vid not found', ['%vid' => $video_id])];
    }
    $video = $list_response[0]['snippet'];
    return ['title' => $video['title'], 'default_thumb' => $video['thumbnails']['high']['url']];
  }

  /**
   * Get google token.
   */
  public function getToken() {
    // Calculate token expiry.
    $token = $this->getConfig('access_token');
    $this->client->setAccessToken($token);
    // And perform required action.
    if ($this->client->isAccessTokenExpired()) {
      // If Token expired.
      // We need to refresh token in this case.
      // Check whether we have a refresh token or not.
      $refreshToken = $this->getConfig('refresh_token');
      if ($refreshToken != NULL) {
        // If refresh token present.
        $this->client->refreshToken($refreshToken);
        $newToken = $this->client->getAccessToken();
        $config = $this->configFactory->getEditable('youtube_uploader.settings');
        $config->set('access_token', $newToken)->save();
      }
      else {
        // If refresh token isn't present.
        $this->client->refreshToken($refreshToken);
        $newToken = $this->client->getAccessToken();
        $config = $this->configFactory->getEditable('youtube_uploader.settings');
        $config->set('access_token', $newToken)->save();
      }
    }
    return $this->getConfig('access_token');
  }

  /**
   * Return config from youtube_uploader config settings.
   */
  protected function getConfig($config) {
    return $this->configFactory->get('youtube_uploader.settings')->get($config);
  }

  /**
   * Revoke an OAuth2 access token or refresh token.
   *
   * This method will revoke current access token, if a token isn't provided.
   */
  public function revokeAuth() {
    return $this->client->revokeToken();
  }

  /**
   * Gets AuthURL.
   */
  public function getAuthUrl() {
    return $this->client->createAuthUrl();
  }

  /**
   * Authorizes client.
   */
  public function authorizeClient($code) {
    $this->client->setAccessType("offline");
    $this->client->authenticate($code);

    // Store token into database.
    $config = $this->configFactory->getEditable('youtube_uploader.settings');
    $config->set('access_token', $this->client->getAccessToken())->save();
    $config->set('refresh_token', $this->client->getRefreshToken())->save();

    drupal_set_message('New Token Authorized!!');
  }

  /**
   * Service to retrive YouTube Account Owner details.
   */
  public function youTubeAccount() {
    try {

      // Will set tokens & refresh token when necessary.
      $this->manageTokens();

      $youtube = new \Google_Service_YouTube($this->client);

      $channelsResponse = $youtube->channels->listChannels(
          'brandingSettings', [
            'mine' => 'true',
          ]
      );

      $branding = $channelsResponse->getItems()[0]->getBrandingSettings();
      $channel = (!empty($branding)) ? $branding->getChannel() : NULL;
      if ($channel == NULL) {
        return FALSE;
      }

      return $channel;
    }
    catch (\Exception $e) {
      drupal_set_message('Youtube Uploader Error : @e', ['@e' => $e->getMessage()], 'error');
    }
  }

}
