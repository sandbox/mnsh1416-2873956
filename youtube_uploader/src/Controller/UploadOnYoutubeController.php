<?php

namespace Drupal\youtube_uploader\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Upload On Youtube controller for Authorization & Revoking user access.
 */
class UploadOnYoutubeController extends ControllerBase {

  protected $ytservice;
  protected $configFactory;
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct($youtube, $configFactory, $request) {
    $this->ytservice = $youtube;
    $this->configFactory = $configFactory;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('youtube_uploader_service'), $container->get('config.factory'), $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    $html = $this->ytservice->uploadVideo();

    return [
      '#markup' => $html,
    ];
  }

  /**
   * Revokes Google Authorization.
   *
   * @param bool $showmsg
   *   (optional) Controls display of authentication revoked message.
   */
  public function revoke($showmsg = TRUE) {
    $config = $this->configFactory->getEditable('youtube_uploader.settings');
    $config->set('access_token', NULL)->save();

    $this->ytservice->revokeAuth();

    if ($showmsg) {
      drupal_set_message($this->t('Authentication Revoked. Need re authorization from Google.'));
    }

    return $this->redirect('youtube_uploader.configform');
  }

  /**
   * Authorizes User.
   */
  public function authorize() {
    // Handles authorization from google.
    $code = $this->request->query->get('code');
    $error = $this->request->query->get('error');
    // Authorize current request.
    $this->ytservice->authorizeClient($code);

    if ($code) {
      if ($this->ytservice->youTubeAccount() === FALSE) {
        drupal_get_messages();
        drupal_set_message($this->t('YouTube account not configured properly.'), 'error');
        $this->revoke(FALSE);
      }
    }
    elseif ($error == 'access_denied') {
      drupal_set_message($this->t('Access Rejected! grant application to use your account.'), 'error');
    }
    // Redirect to configform.
    return $this->redirect('youtube_uploader.configform');
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {

    $config = $this->config('youtube_uploader.settings');
    $data = $this->ytservice->getToken();
    $default_privacy = $config->get('yt_default_privacy');
    $allowed_exts = $config->get('allowed_exts');
    $max_upload = $config->get('max_youtube_uploader');
    $token = [
      'access_token' => $data['access_token'],
      'default_privacy' => $default_privacy,
      'allowed_exts' => $allowed_exts,
      'max_youtube_uploader' => $max_upload,
    ];
    return new JsonResponse($token);
  }

}
