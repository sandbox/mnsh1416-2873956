<?php

/**
 * @file
 * Drush integration for select box.
 */

/**
 * The Youtube Uploader Library.
 */
define('YOUTUBE_UPLOADER_DOWNLOAD_URI', 'https://raw.githubusercontent.com/youtube/api-samples/master/javascript/cors_upload.js');
define('YOUTUBE_UPLOADER_DIRECTORY', 'youtube-uploader');

/**
 * Implements hook_drush_command().
 */
function youtube_uploader_drush_command() {
  $items = [];

  // The key in the $items array is the name of the command.
  $items['youtube-uploader-plugin'] = [
    'callback' => 'drush_youtube_uploader_plugin',
    'description' => dt('Download and install the Youtube Uploader plugin.'),
    // No bootstrap.
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => [
      'path' => dt('Optional. A path where to install the Youtube Uploader plugin. If omitted Drush will use the default location.'),
    ],
    'aliases' => ['youtubeuploaderplugin'],
  ];

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 */
function youtube_uploader_drush_help($section) {
  switch ($section) {
    case 'drush:youtube-uploader-plugin':
      return dt('Download and install the Youtube Uploader Library from https://raw.githubusercontent.com/youtube/api-samples/master/javascript/cors_upload.js, default location is the libraries directory.');
  }
}

/**
 * Command to download the Youtube Uploader plugin.
 */
function drush_youtube_uploader_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'libraries';
  }


  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', ['@path' => $path]), 'notice');
  }
  
  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);
  
  
  // Create the 'youtube-uploader' directory if it does not exist.
  $yt = 'youtube-uploader';
  if (!is_dir($yt)) {
    drush_op('mkdir', $yt);
    drush_log(dt('Directory @yt was created', ['@yt' => $yt]), 'notice');
  }

  chdir($yt);

  // Download the required cors file.
  if ($filepath = drush_download_file(YOUTUBE_UPLOADER_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    drush_log(dt('@fn downloaded to @yt', ['@fn' => $filename, '@yt' => $yt]), 'notice');
    drush_move_dir($filepath, $yt);
    rename('youtube-uploader', 'cors.js');
    drush_log(dt('Youtube Uploader plugin (cors_upload.js) has been downloaded to @path', ['@path' => $path. '/' . $yt]), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Youtube Uploader plugin (cors_upload.js) to @path', ['@path' => $path. '/' . $yt]), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
